import { View, Text, Pressable, TouchableOpacity } from "react-native";
import React, { useLayoutEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { SafeAreaView } from "react-native-safe-area-context";

const HomeScreen = () => {
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  return (
    <SafeAreaView>
      <View>
        <View>
          <View>
            <Text style={{ fontSize: 44 }}>LUPUS IN TABULA</Text>
          </View>

          <View>
            <Text style={{ fontSize: 25 }}>
              Fare il master non è mai stato così facile.
            </Text>
          </View>

          <View>
            <Pressable>
              <Text style={{ fontSize: 24 }}>INCOMINCIAMO</Text>
            </Pressable>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate("MainScreen")}>
            <Text>Go to Profile Screen</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
