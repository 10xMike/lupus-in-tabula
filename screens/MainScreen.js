import React from "react";
import { useNavigation } from "@react-navigation/native";
import { View, Text, TouchableOpacity } from "react-native";

const MainScreen = () => {
  const navigation = useNavigation();

  return (
    <View>
      <Text>Main Screen</Text>
      <TouchableOpacity onPress={() => navigation.navigate("Home")}>
        <Text>Go to Home Screen</Text>
      </TouchableOpacity>
    </View>
  );
};

export default MainScreen;
